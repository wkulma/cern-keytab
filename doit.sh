#!/bin/bash
KEYTAB="/$1/krb5.keytab"
if [ -e $KEYTAB ]; then
	echo "destination $KEYTAB already exists, won't touch it..."
	exit 0
fi

XMLCURL=$(curl -s -k --local-port 600-700 "https://lxkerbwin.cern.ch/LxKerb.asmx/ResetComputerPasswordTimeCheck?service=host&t=$(date +%FT%T)")
PASSWORD=$(echo "$XMLCURL" | xmllint --xpath "//*[local-name()='keytabfile']/*[local-name()='computerpassword']/text()" -)
CERNDC=$(dig -t SRV _kerberos._tcp.cern.ch | grep ^cerndc | awk 'NR==1{print $1}')

sleep 15 # seems to be required, error otherwise
echo "creating new keytab :: using ${CERNDC}"
msktutil --update --dont-expire-password --server $CERNDC --computer-name $(hostname -s) --service host --old-account-password $PASSWORD
if [ $? -ne 0 ]; then
	echo -n "failed to created keytab"
	exit -1
fi

echo "keytab created... copying to $1"
cp /etc/krb5.keytab $1
exit $?
