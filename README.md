# What's in this image

This image sets up the host keytab for a CERN host.

## Features

# Usage

You must run it with --net host (we need the actual hostname), and bind mount /etc into the container.

The destination of the bind mount in the container should be passed as an argument (the keytab gets copied there).

Here's a sample usage of the container image:
```
docker run --name cern-keytab --rm --net host \
	-v /etc:/host-etc \
	gitlab-registry.cern.ch/cloud/cern-keytab /host-etc
```

# Issues

# Contributing

You are invited to contribute new features and fixes.

[Check here](https://gitlab.cern.ch/cloud/cern-keytab) for the original Dockerfile and repository.

# Authors

Ricardo Rocha [ricardo.rocha@cern.ch](mailto:ricardo.rocha@cern.ch)

