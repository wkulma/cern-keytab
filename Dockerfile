FROM fedora:23

MAINTAINER Ricardo Rocha <ricardo.rocha@cern.ch>

RUN dnf install -y \
	bind-utils \
	curl \
	krb5-workstation \
	hostname \
	msktutil

ADD krb5.conf /etc/krb5.conf

ADD doit.sh /doit.sh

ENTRYPOINT ["/doit.sh"]
